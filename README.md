# Final leaderboard

|Name|R1(1)|R1(2)|R2(1)|R2(2)|R3(1)|R3(2)|Total|
|----|------|----|------|----|------|---|-----|
|ThomJ|10593|9954|93130|93725|236002|214253|657657|
|SamBilson|11679|9708|79605|82940|207594|247888|639414|
|AlexMarkham|10056|10061|87963|87312|233648|208528|637568|
|JacobHutchings|10957|9739|102852|79905|217212|208440|629105|
|DanStaff|11092|9666|97749|80675|237940|181672|618794|
|stephenc|10153|9706|81077|75500|231798|209865|618099|
|DaveP|11258|9726|85901|77625|206135|215345|605990|
|TimH|11445|11682|79785|79650|208116|207729|598407|
|Graham|10776|11394|80586|79899|207321|207111|597087|
|lauren|9142|9894|86945|74411|186087|221954|588433|
|AmyCunliffe|10183|10757|79863|81929|220030|184513|587275|
|AndrewWoodford|10489|9201|82471|91882|174281|215567|583891|
|LewisBall|8095|8616|83960|83114|212389|182971|579145|
|GeorgiaVann|10079|10205|92061|80603|194752|187281|574981|
|TimBazalgette|10137|9479|82859|79473|197801|193803|573552|
|PaulJ|9625|9895|83627|83862|182104|192474|561587|
|ChrisOgilvie|10207|9709|89254|83966|181231|185276|559643|
|indra|10662|9220|80830|90308|179187|182676|552883|
|MarkElsey|9887|9975|81839|85921|166205|172067|525894|
|MattSummers|10241|11265|81829|79158|169376|167416|519285|
|HenryStanford|11697|8814|80115|79320|168208|165779|513933|


# Gecko Locator's Xmas Quiz Game
## 20 December, 2016

Welcome to Gecko Locator's Xmas Quiz game.

We are going to have a game of Prisoner's Dilemma.

![alt-tag](https://i.makeagif.com/media/6-21-2016/St1755.gif)

The rules are as follows:

- For each game, players are randomly drawn into pairs.
- Each player decides whether to SHARE or STEAL.
- If both players in a pair SHARE, they each receive 2 points.
- If both players in a pair STEAL, they each receive 0 points.
- If one player SHARES and the other STEALS, the sharer receives 0 points and the stealer receives 3 points.

We are going to play a very large number of games, so you will create some players that will act on your behalf. 

Players look like this:

```json
{
  "name": "Alice",
  "threshold": 50.0,
  "share_low": 40.0,
  "share_high": 65.0
}

{
  "name": "Bob",
  "threshold": 30.0,
  "share_low": 50.0,
  "share_high": 60.0
}
```

When Alice plays Bob, Alice follows these rules to decide what to do:

- Alice looks at Bob's share percentage in previous games, i.e. the percentage of all Bob's previous games where he chose to SHARE.
- If Bob's share percentage is stricly less than Alice's `threshold`, then Alice chooses to SHARE with probability `share_low`% (i.e. 40%), and to STEAL with probability (100 - `share_low`)% (i.e. 60%).
- If Bob's share percentage is greater than or equal to Alice's `threshold`, then Alice chooses to SHARE with probability `share_high`% (i.e. 65%), and to STEAL with probability (100 - `share_high`)% (i.e. 35%).

(For the first game, everyone starts with a share percentage of 100%).

If you wish to play, please do the following:
- Provide me with **two** players, formatted exactly as shown above.
- Name your players `<NAME>_1` and `<NAME>_2`, using your actual name (no pseudonyms, please).
- You are free to choose your threshold and share percentages as you like, so long as they are all numbers between 0 and 100 inclusive.

### Round 1

We will begin with 10,000 games. After these games have taken place, I will make available the full list of players with their scores and strategies.

### Round 2

You can then choose either to leave your players as they are, or to change their strategies. Depending on how things are going, I may add some players of my own to make things interesting - but they obviously won't be eligible to win, and I'll let you know before I do it.

We will play a further 100,000 games. Share and steal counts will be reset to 0. I will again make available the list of players, scores and strategies.

### Round 3

You will then have one more opportunity to change your players' strategies if you wish to do so.

We will play a further 200,000 games. Again, share and steal counts will be reset to 0.

__The winner will be the entrant whose players scored the largest number of points over the three rounds.__

All being well, we will stick to the following timetable:

| Time  |                                                       |
| ----- | ----------------------------------------------------- |
| 14:20 | Initial deadline for submitting players               | 
| 14:30 | Round 1 results released                              |
| 14:50 | Round 2 deadline for changing player strategies       |
| 15:00 | Round 2 results released                              |
| 15:20 | Round 3 deadline for changing player strategies       |
| 15:30 | Winner announced                                      |