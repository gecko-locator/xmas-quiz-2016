#!/usr/bin/env python3

import matplotlib.pyplot as plt
import json

with open('results.json') as f:
    results = json.load(f)

scores = [result['score'] for result in results]
share_high = [result['profile']['share_low'] for result in results]
share_low = [result['profile']['share_high'] for result in results]
thresholds = [result['profile']['threshold'] for result in results]

plt.scatter(share_high, scores, 25, 'g')
plt.scatter(share_low, scores, 25, 'b')
plt.scatter(thresholds, scores, 25, 'r')
plt.show()
