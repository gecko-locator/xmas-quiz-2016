#!/usr/bin/env python3

import sys
import random
import json

SHARE = 2
STEAL = 3

class Player:
    def __init__(self, share_low, share_high, threshold, name):
        self._share_low = share_low
        self._share_high = share_high
        self._threshold = threshold
        self._name = name
        self.score = 0
        self.shares = 0
        self.steals = 0
        assert 0 <= share_low <= 1
        assert 0 <= share_high <= 1
        assert 0 <= threshold <= 1
    def share_prop(self):
        try:
            return self.shares / (self.shares + self.steals)
        except ZeroDivisionError:
            return 1.0
    def share(self, other):
        if other.share_prop() < self._threshold:
            return random.uniform(0, 1) < self._share_low
        return random.uniform(0, 1) < self._share_high
    def stats(self):
        return {
            'profile': {
                'share_low': self._share_low * 100,
                'share_high': self._share_high * 100,
                'threshold': self._threshold * 100,
                'name': self._name
            },
            'score': self.score,
            'share%': self.share_prop() * 100,
            'shares': self.shares,
            'steals': self.steals
        }

def read_player_prof(profile):
    return Player(profile['share_low'] * 0.01,
                  profile['share_high'] * 0.01,
                  profile['threshold'] * 0.01,
                  profile['name'])


def play(p1, p2):
    share1 = p1.share(p2)
    share2 = p2.share(p1)
    if share1:
        p1.shares += 1
        if share2:
            p2.shares += 1
            p1.score += SHARE
            p2.score += SHARE
        else:
            p2.steals += 1
            p2.score += STEAL
    else:
        p1.steals += 1
        if share2:
            p2.shares += 1
            p1.score += STEAL
        else:
            p2.steals += 1

def play_round(players):
    random.shuffle(players)
    for p1, p2 in zip(players[::2], players[1::2]):
        play(p1, p2)

def play_rounds(players, n_rounds):
    for _ in range(n_rounds):
        play_round(players)

def main():
    with open(sys.argv[1]) as f:
        profiles = json.load(f)
    random.seed(int(sys.argv[3]))
    players = list(map(read_player_prof, profiles))
    assert len(players) % 2 == 0
    play_rounds(players, int(sys.argv[2]))
    player_stats = sorted((player.stats() for player in players),
                          key=lambda p: p['score'])
    print(json.dumps(player_stats, sort_keys=True))

if __name__ == "__main__":
    main()
